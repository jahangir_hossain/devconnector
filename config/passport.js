const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const mongoose = require('mongoose');
const User = mongoose.model('users');
const keys = require('./keys');


const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = keys.secretOrKey;

module.exports = passport => {
    passport.use(
        new JwtStrategy(opts, (jwt_payload, done) =>{
            // Find the user in MongoDB which is included in payload
            User.findById(jwt_payload.id)
                .then(user =>{
                    if(user){
                        return done(null, user); // err -> null
                    }
                    return done(null, false);
                })
                .catch(err => console.log(err));


        })
    );

};