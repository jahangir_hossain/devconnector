const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

// Load validation 

const validateProfileInput = require('../../validation/profile');
const validateExperienceInput = require('../../validation/experience');
const validateEducationInput = require('../../validation/education');

// Load  Profile Model
const Profile = require('..//../models/Profile');

//Load User Model
const User = require('..//../models/User');

// @route  GET /api/profile/test
// @desc   Tests profile route
// @access public
router.get('/test', (req, res) => res.json({msg: "profile works"}));

// @route   Get api/profile
// @desc    Get current user's profile
// @access  private
router.get('/', passport.authenticate('jwt', {session: false}), (req, res)=>{
    const errors ={};
    Profile.findOne({user: req.user.id })
        .then(profile => {
            if(!profile){
                errors.noProfile = "there is no profile for this user";
                return res.status(404).json(errors);
            }
            res.json(profile);
        })
        .catch(err => res.status(404).json(err));
});

// @route GET /api/profile/all
// @desc Get all profiles
// @access public
router.get('/all', (req, res) => {
    const errors = {};
    
    Profile.find()
        .populate('users', ['name', 'avatar'])
        .then(profiles => {
            if(!profiles){
                errors.noprofile = 'there are no profiles'
                res.status(404).json();
            }

            res.json(profiles);
        })
        .catch(err => res.status(404).json({profile: 'there are no profile'}));
});

// @route  GET /api/profile/handle/:handle
// @desc   Get profile by handle
// @access public
router.get('/handle/:handle', (req, res) => {
    const errors = {};

    Profile.findOne({handle: req.params.handle})
        .populate('users', ['name', 'avatar'])
        .then(profile => {
            if(!profile){
                errors.noprofile = 'there is no profile for this user';
                res.status(404).json(errors);
            }

            res.json(profile);
        })
        .catch(err => res.status(404).json({profile: 'there is no profile for this user'}));
});

// @route  GET /api/profile/user/:user_id
// @desc   Get profile by handle
// @access public
router.get('/user/:user_id', (req, res) => {
    const errors = {};

    Profile.findOne({user: req.params.user_id})
        .populate('users', ['name', 'avatar'])
        .then(profile => {
            if(!profile){
                errors.noprofile = 'there is no profile for this user';
                res.status(404).json(errors);
            }

            res.json(profile);
        })
        .catch(err => res.status(404).json({profile: 'there is no profile for this user'}));
});

// @route   POST api/profile
// @desc    Create / Update profile
// @access  Private
router.post('/', passport.authenticate('jwt', {session: false}), (req, res) => {
    //validating profile fields
    const {errors, isValid} = validateProfileInput(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }
    //Get fields
    const profilefields = {};
    profilefields.user = req.user.id;
    if(req.body.handle) profilefields.handle = req.body.handle;
    if(req.body.company) profilefields.company = req.body.company;
    if(req.body.website) profilefields.website = req.body.website;
    if(req.body.location) profilefields.location = req.body.location;
    if(req.body.bio) profilefields.bio = req.body.bio;
    if(req.body.status) profilefields.status = req.body.status;
    if(req.body.githubusername) profilefields.githubusername = req.body.githubusername;

    //Skills - split into array
    if(typeof req.body.skills !== 'undefined'){
        profilefields.skills = req.body.skills.split(',');
    } 

    //Social
    profilefields.social = {};
    if(req.body.youtube) profilefields.social.youtube = req.body.youtube;
    if(req.body.twitter) profilefields.social.twitter = req.body.twitter;
    if(req.body.facebook) profilefields.social.facebook = req.body.facebook;
    if(req.body.linkedin) profilefields.social.linkedin = req.body.linkedin;
    if(req.body.instagram) profilefields.social.instagram = req.body.instagram;
    // Create or Update
    Profile.findOne({ user: req.user.id })
        .then(profile => {
            if(profile){
                //Update
                Profile.findOneAndUpdate(
                    {user: req.user.id},
                    {$set: profilefields},
                    {new: true}
                ).then( profile => {
                    res.json(profile);
                }).catch(err => res.json(err));

            }else{
                //Create

                //check if handle exist
                Profile.findOne({handle: profilefields.handle}).then(profile => {
                    if(profile){
                        errors.handle = 'That handle already exists';
                        res.status(400).json(errors);
                    }

                    //Save profile
                    new Profile(profilefields).save().then(profile => res.json(profile));
                })
            }
        })

});

// @route POST api/profile/experience
// @desc Add experience to the profile
// @access private
router.post('/experience', passport.authenticate('jwt', {session: false}), (req, res) => {
    //validating experience fields
    const {errors, isValid} = validateExperienceInput(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }
    Profile.findOne({user: req.user.id})
        .then(profile => {
            const newExp = {
                title: req.body.title,
                company: req.body.company,
                location: req.body.location,
                from: req.body.from,
                to: req.body.to,
                current: req.body.current,
                description: req.body.description
            }

            //Add to experience array
            profile.experience.unshift(newExp);

            profile.save().then(profile => res.json(profile));
        })
});

// @route POST api/profile/education
// @desc Add education to the profile
// @access private
router.post('/education', passport.authenticate('jwt', {session: false}), (req, res) => {
    //validating education fields
    const {errors, isValid} = validateEducationInput(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }
    Profile.findOne({user: req.user.id})
        .then(profile => {
            const newEdu = {
                school: req.body.school,
                degree: req.body.degree,
                fieldofstudy: req.body.fieldofstudy,
                from: req.body.from,
                to: req.body.to,
                current: req.body.current,
                description: req.body.description
            }

            //Add to education array
            profile.education.unshift(newEdu);

            profile.save().then(profile => res.json(profile));
        })
});

// @route DELETE api/profile/experience/:exp_id
// @desc Delete experience from profile
// @access private
router.delete('/experience/:exp_id', passport.authenticate('jwt', {session: false}), (req, res) => {
    Profile.findOne({user: req.user.id})
        .then(profile => {
            const removeIndex = profile.experience
                .map(item => item.id)
                .indexOf(req.params.exp_id);

            // Splice out of array
            if(removeIndex >= 0){
                profile.experience.splice(removeIndex, 1);
            }
            

            // Save
            profile.save().then(profile => res.json(profile));

        })
});


// @route DELETE api/profile/education/:edu_id
// @desc Delete education from profile
// @access private
router.delete('/education/:edu_id', passport.authenticate('jwt', {session: false}), (req, res) => {
    Profile.findOne({user: req.user.id})
        .then(profile => {
            const removeIndex = profile.education
                .map(item => item.id)
                .indexOf(req.params.edu_id);

                console.log("Remove Index: ",removeIndex);

            // Splice out of array
            if(removeIndex >= 0){
                profile.education.splice(removeIndex, 1);
            }

            // Save
            profile.save().then(profile => res.json(profile));

        })
});


// @route  DELETE api/profile
// @desc   Delete user and profile
// @access Private
router.delete('/', passport.authenticate('jwt', {session: false}), (req, res) => {
    Profile.findOneAndRemove({user: req.user.id})
        .then(() => {
            User.findOneAndRemove({_id: req.user.id})
                .then(()=> {
                    res.json({success: true});
                })
        }).catch(err => res.status(404).json({success: false}))
});

module.exports = router;