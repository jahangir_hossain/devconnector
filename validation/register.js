const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = validateRegisterInput = data => {
    let errors = {};

    data.name = isEmpty(data.name) ?  '' : data.name;
    data.email = isEmpty(data.email) ?  '' : data.email;
    data.password = isEmpty(data.password) ?  '' : data.password;
    data.confirmpassword = isEmpty(data.confirmpassword) ?  '' : data.confirmpassword;

    if(!Validator.isLength(data.name, {min: 2, max: 30})){
        errors.name = "Name must be between 2 to 30 characters";
    }

    if(Validator.isEmpty(data.name)){
        errors.name = "Name Field is required";
    }

    if(!Validator.isEmail(data.email)){
        errors.email = "invalid email";
    }

    if(Validator.isEmpty(data.email)){
        errors.email = "Email is required";
    }

    if(!Validator.isLength(data.password, {min: 6, max: 30})){
        errors.password = "Password must be between 6 to 30";
    }

    if(Validator.isEmpty(data.password)){
        errors.password = "Password is required";
    }

    if(!Validator.equals(data.password, data.confirmpassword)){
        errors.confirmpassword = "Password must match";
    }

    if(Validator.isEmpty(data.confirmpassword)){
        errors.confirmpassword = "confirmpassword is required";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}